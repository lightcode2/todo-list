import dayjs from "dayjs";
import { useRef, useState, useContext } from "react";
import styles from "./styles.module.less";
import { RequestStatusContext } from "../../App";
import AttachmentIcon from "../../icons/AttachmentIcon";
import InteractiveImage from "../interactiveImage";

/**
 * Свойства задачи, которые будут использоваться по умолчанию
 */
const defaultStateValues = {
  title: "",
  description: "",
  expiresOn: dayjs().add(1, "day").toISOString().split("T")[0],
  status: "active",
  images: [],
};

/**
 * Компонент формы для создания/изменения задачи
 *
 * @module
 * @function TaskForm
 * @param {Object} props
 * @param {Object} props.data - данные о задаче. Если не переданы, то будут использоваться
 * значения по умолчанию из {@link defaultStateValues}
 * @param {Function} props.onSubmitHandler - callback, который будет вызван при сабмите формы
 *
 */
export default function TaskForm({
  data = defaultStateValues,
  onSubmitHandler,
}) {
  const { isPending } = useContext(RequestStatusContext);
  const [title, setTitle] = useState(data.title);
  const [description, setDescription] = useState(data.description);
  const [expiresOn, setExpiresOn] = useState(data.expiresOn);
  const [status] = useState(data.status);
  const [uploadedImages, setUploadedImages] = useState(data.images);

  const imageInputRef = useRef();

  const updateForm = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    if (name === "title") {
      setTitle(value);
    }
    if (name === "description") {
      setDescription(value);
    }
    if (name === "expiration") {
      setExpiresOn(value);
    }
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();

    const formData = {
      title,
      description,
      expiresOn,
      status,
      images: uploadedImages,
    };

    onSubmitHandler(formData);
  };

  const handleAttachmentIconClick = () => {
    imageInputRef.current.click();
  };

  const appendUploadedImage = (newImage) => {
    setUploadedImages([...uploadedImages, newImage]);
  };

  const handleFileUpload = (e) => {
    const file = e.currentTarget.files[0];

    const fileReader = new FileReader();

    fileReader.readAsDataURL(file);

    fileReader.onload = () => {
      const uploadedFile = {
        id: file.name + file.size,
        source: fileReader.result,
      };

      appendUploadedImage(uploadedFile);
    };

    fileReader.onabort = () => {
      alert("reading a file aborted!");
    };

    fileReader.onerror = () => {
      alert("An error occured during reading a file");
    };
  };

  const handleRemoveUploadedImage = (id) => {
    const result = uploadedImages.filter((image) => image.id !== id);
    setUploadedImages(result);
  };

  return (
    <form className={styles.taskForm} onSubmit={handleFormSubmit}>
      <label className={styles.formLabel}>
        Заголовок
        <input name="title" value={title} onChange={updateForm} />
      </label>

      <label className={styles.formLabel}>
        Описание
        <input name="description" value={description} onChange={updateForm} />
      </label>

      <div className={styles.twoColumnBlock}>
        <label>
          Выполнить до
          <input
            type="date"
            name="expiration"
            value={expiresOn}
            onChange={updateForm}
          />
        </label>

        <div className={styles.attachmentBlock}>
          <span>Прикрепить фото</span>
          <button type="button" onClick={handleAttachmentIconClick}>
            <AttachmentIcon color="#0284c7" />
          </button>
          <input
            type="file"
            alt="attach image"
            ref={imageInputRef}
            onChange={handleFileUpload}
          ></input>
        </div>
      </div>

      <button type="submit" disabled={isPending}>
        {isPending ? "Отправка..." : "Отправить"}
      </button>

      <div className={styles.uploadedImagesBlock}>
        {uploadedImages.map((image) => (
          <InteractiveImage
            source={image.source}
            id={image.id}
            key={image.id}
            onClickHandler={handleRemoveUploadedImage}
          />
        ))}
      </div>
    </form>
  );
}
