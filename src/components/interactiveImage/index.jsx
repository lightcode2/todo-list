import styles from "./styles.module.less";
import RemoveIcon from "../../icons/RemoveIcon";

/**
 * Callback, который удаляет изображение с переданным ему id
 *
 * @callback onClickHandler
 * @param {string} id - идентификатор задачи
 */

/**
 * Компонент, который отображает, а также позволяет удалять при клике переданное ему изображение
 * @module
 * @function InteractiveImage
 * @param {Object} props
 * @param {string} props.source - изображение, которое должно быть отображено в компоненте
 * @param {string} props.id - идентификатор изображения
 * @param {onClickHandler} props.onClickHandler
 *
 */
export default function InteractiveImage({ source, id, onClickHandler }) {
  return (
    <div className={styles.interactiveImage}>
      <img src={source} alt="thumb" />
      <div className={styles.cover} onClick={() => onClickHandler(id)}>
        <RemoveIcon color="#FFF" />
      </div>
    </div>
  );
}
