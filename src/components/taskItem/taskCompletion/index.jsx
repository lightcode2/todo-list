import styles from "./styles.module.less";
import ActiveIcon from "../../../icons/ActiveIcon";
import DoneIcon from "../../../icons/DoneIcon";

/**
 * Компонент для отображения одной конкретной задачи
 * @module
 * @function TaskCompletion
 * @param {Object} props
 * @param {string} props.status - статус задачи
 * @param {Function} props.handleChangeStatus - callback
 *
 */
export default function TaskCompletion({ status, handleChangeStatus }) {
  return (
    <div className={styles.taskCompletion}>
      {status === "done" ? (
        <div onClick={() => handleChangeStatus("active")}>
          <DoneIcon />
        </div>
      ) : (
        <div onClick={() => handleChangeStatus("done")}>
          <ActiveIcon />
        </div>
      )}
    </div>
  );
}
