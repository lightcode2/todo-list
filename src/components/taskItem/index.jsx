import { db } from "../../firebase-config";
import { updateDoc, doc } from "firebase/firestore";
import { useContext, useState } from "react";
import { ListContext, RequestStatusContext } from "../../App";
import TaskInfo from "./taskInfo";
import TaskForm from "../taskForm";
import { getTasks } from "../../utils";
import styles from "./styles.module.less";

/**
 * Callback, который изменяет текущую задачу и вызывает обновление всего списка задач
 *
 * @callback handleFormSubmit
 * @param {Object} data - данные о задаче
 */

/**
 * Callback, который переключает режим "просмотра" задачи на "редактирование" и обратно
 *
 * @callback handleSwitchEditMode
 */

/**
 * Компонент для отображения одной конкретной задачи
 * @module
 * @function TaskItem
 * @param {Object} props
 * @param {Object} props.data - данные о задаче
 *
 */
export default function TaskItem({ data }) {
  const { setIsPending } = useContext(RequestStatusContext);
  const { setTasks } = useContext(ListContext);
  const [isEditModeOn, setIsEditModeOn] = useState(false);

  const handleFormSubmit = async (formData) => {
    const taskDocument = doc(db, "tasks", data.id);

    const updatedTaskData = formData;

    setIsPending(true);

    try {
      await updateDoc(taskDocument, updatedTaskData);
      const dbTasks = await getTasks();
      setTasks(dbTasks);
      setIsEditModeOn(false);
    } catch (err) {
      console.log("an error occured during creating a new task");
    } finally {
      setIsPending(false);
    }
  };

  const handleSwitchEditMode = () => {
    setIsEditModeOn(!isEditModeOn);
  };

  return (
    <div>
      {isEditModeOn ? (
        <div className={styles.taskItem}>
          <button type="button" onClick={handleSwitchEditMode}>
            Отменить
          </button>
          <TaskForm data={data} onSubmitHandler={handleFormSubmit} />
        </div>
      ) : (
        <TaskInfo
          data={data}
          onSubmitHandler={handleFormSubmit}
          onSwitchEditModehandler={handleSwitchEditMode}
        />
      )}
    </div>
  );
}
