import { useContext } from "react";
import { db } from "../../../firebase-config";
import { deleteDoc, doc } from "firebase/firestore";
import styles from "./styles.module.less";
import { ListContext } from "../../../App";
import EditIcon from "../../../icons/EditIcon";
import RemoveIcon from "../../../icons/RemoveIcon";
import { getTasks } from "../../../utils";

/**
 * Компонент для отображения одной конкретной задачи
 * @module
 * @function TaskControls
 * @param {Object} props
 * @param {string} props.id - идентификатор задачи
 * @param {Function} props.switchEditModeHandler - callback, переключающий состояние isEditModeOn
 *
 */
export default function TaskControls({ id, switchEditModeHandler }) {
  const { setTasks } = useContext(ListContext);
  const handleRemoveTask = async () => {
    const taskDocument = doc(db, "tasks", id);
    await deleteDoc(taskDocument);

    const dbTasks = await getTasks();
    setTasks(dbTasks);
  };

  return (
    <div className={styles.controls}>
      <div onClick={switchEditModeHandler}>
        <EditIcon />
      </div>
      <div onClick={handleRemoveTask}>
        <RemoveIcon />
      </div>
    </div>
  );
}
