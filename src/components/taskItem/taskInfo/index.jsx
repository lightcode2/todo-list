import dayjs from "dayjs";
import { useEffect, useContext, useState } from "react";
import { ModalContext } from "../../../App";
import styles from "./styles.module.less";
import TaskControls from "../taskControls";
import TaskCompletion from "../taskCompletion";

/**
 * Компонент, который отображает все данные о задаче, а также элементы управления задачей
 *
 * @module
 * @function TaskInfo
 *
 * @param {Object} props
 * @param {Oject} props.data - данные о задаче
 * @param {handleFormSubmit} props.onSubmitHandler
 * @param {handleSwitchEditMode} props.onSwitchEditModehandler
 */
export default function TaskInfo({
  data,
  onSubmitHandler,
  onSwitchEditModehandler,
}) {
  const [taskStatus, setTaskStatus] = useState();

  const { setModalImage, setIsModalOpen } = useContext(ModalContext);
  useEffect(() => {
    if (data.status !== "done" && dayjs().isAfter(data.expiresOn)) {
      setTaskStatus("expired");
    } else {
      setTaskStatus(data.status);
    }
  }, [data.status, data.expiresOn]);

  const handleChangeStatus = (status) => {
    const formData = { ...data, status };
    onSubmitHandler(formData);
  };

  const handleShowInModal = (image) => {
    setIsModalOpen(true);
    setModalImage(image);
  };

  return (
    <div className={`${styles.taskItem} ${styles[taskStatus]}`}>
      <div className={styles.topBlock}>
        <div className={styles.taskInfo}>
          <h3>{data.title}</h3>
          <p>{data.description}</p>
        </div>
        {taskStatus !== "expired" && (
          <TaskCompletion
            status={data.status}
            handleChangeStatus={handleChangeStatus}
          />
        )}
      </div>
      <div className={styles.bottomBlock}>
        <span>до {dayjs(data.expiresOn).format("DD/MM/YYYY")}</span>
        <TaskControls
          id={data.id}
          switchEditModeHandler={onSwitchEditModehandler}
        />
      </div>

      {data.images.length > 0 && (
        <div className={styles.imagesBlock}>
          {data.images.map((image, idx) => (
            <img
              src={image.source}
              alt="pic"
              key={idx}
              onClick={() => handleShowInModal(image.source)}
            />
          ))}
        </div>
      )}
    </div>
  );
}
