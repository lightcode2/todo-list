import { useContext } from "react";
import styles from "./styles.module.less";
import { ModalContext } from "../../App";
import RemoveIcon from "../../icons/RemoveIcon";

/**
 * Модальное окно, которое показывает выбранное изображение
 * @module
 * @function ImageModal
 */
export default function ImageModal() {
  const {
    modalImage: image,
    isModalOpen: isOpen,
    setIsModalOpen,
  } = useContext(ModalContext);

  if (!isOpen) return null;

  const onCloseHandler = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <div className={styles.overlay} onClick={onCloseHandler}></div>
      <div className={styles.modalWindow}>
        <button type="button" onClick={onCloseHandler}>
          <RemoveIcon color="#000" />
        </button>

        <img src={image} alt="task" />
      </div>
    </>
  );
}
