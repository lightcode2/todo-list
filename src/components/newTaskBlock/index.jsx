import { useState, useContext } from "react";
import { ListContext, RequestStatusContext } from "../../App";
import styles from "./styles.module.less";
import TaskForm from "../taskForm";
import MinusIcon from "../../icons/MinusIcon";
import PlusIcon from "../../icons/PlusIcon";
import { db } from "../../firebase-config";
import { collection, addDoc } from "firebase/firestore";
import { getTasks } from "../../utils";

/**
 * Блок, в котором при необходимости будет появляться форма для создания новой задачи
 * @module
 * @function NewTaskBlock
 *
 */
export default function NewTaskBlock() {
  const { setIsPending } = useContext(RequestStatusContext);
  const { setTasks } = useContext(ListContext);
  const [isFormOpen, setIsFormOpen] = useState(false);

  const tasksCollectionRef = collection(db, "tasks");

  const switchNewTaskState = () => {
    setIsFormOpen(!isFormOpen);
  };

  const handleFormSubmit = async (formData) => {
    setIsPending(true);
    try {
      await addDoc(tasksCollectionRef, formData);
      const dbTasks = await getTasks();
      setTasks(dbTasks);
      setIsFormOpen(false);
    } catch (err) {
      console.log("an error occured during creating a new task");
    } finally {
      setIsPending(false);
    }
  };

  return (
    <div className={styles.newTaskBlock}>
      <div className={styles.header}>
        <span>Добавить новую задачу</span>
        <button type="button" onClick={switchNewTaskState}>
          {isFormOpen ? <MinusIcon /> : <PlusIcon />}
        </button>
      </div>
      {isFormOpen && <TaskForm onSubmitHandler={handleFormSubmit} />}
    </div>
  );
}
