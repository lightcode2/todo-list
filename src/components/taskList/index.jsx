import { useContext, useEffect } from "react";
import { ListContext } from "../../App";
import styles from "./styles.module.less";
import TaskItem from "../taskItem";
import { getTasks } from "../../utils";

/**
 * Компонент для отображения списка задач.
 * Каждая задача в списке представляет собой компонент {@link TaskItem}
 *
 * @module
 * @function TaskList
 *
 */
export default function TaskList() {
  const { tasks, setTasks } = useContext(ListContext);

  useEffect(() => {
    const getTasksFromServer = async () => {
      const dbTasks = await getTasks();
      setTasks(dbTasks);
    };

    getTasksFromServer();
  }, [setTasks]);

  return (
    <ul className={styles.taskList}>
      {tasks.map((task) => (
        <li key={task.description}>
          <TaskItem data={task} />
        </li>
      ))}
    </ul>
  );
}
