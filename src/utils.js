import { db } from "./firebase-config";
import { collection, getDocs } from "firebase/firestore";

export const getTasks = async () => {
  const tasksCollectionRef = collection(db, "tasks");
  // возвращает все записи из указанной коллекции
  const data = await getDocs(tasksCollectionRef);

  const dbTasks = data.docs.map((doc) => ({ ...doc.data(), id: doc.id }));

  console.log(dbTasks);
  return dbTasks;
};
