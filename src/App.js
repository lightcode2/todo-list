import { createContext, useState } from "react";
import "./App.less";
import NewTaskBlock from "./components/newTaskBlock";
import TaskList from "./components/taskList";
import ImageModal from "./components/imageModal";

export const ModalContext = createContext();
export const ListContext = createContext();
export const RequestStatusContext = createContext();

function App() {
  const [modalImage, setModalImage] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [tasks, setTasks] = useState([]);
  const [isPending, setIsPending] = useState(false);

  const modalContextValue = {
    modalImage,
    setModalImage,
    isModalOpen,
    setIsModalOpen,
  };

  const listContextValue = { tasks, setTasks };

  const requestStatusContextValue = { isPending, setIsPending };

  return (
    <ModalContext.Provider value={modalContextValue}>
      <RequestStatusContext.Provider value={requestStatusContextValue}>
        <div className="App">
          <ListContext.Provider value={listContextValue}>
            <NewTaskBlock />
            <TaskList />
          </ListContext.Provider>
        </div>
      </RequestStatusContext.Provider>
      <ImageModal />
    </ModalContext.Provider>
  );
}

export default App;
