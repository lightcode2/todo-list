import { initializeApp } from "firebase/app";
import { getFirestore } from "@firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyClBXfFnciV6iYJY-ZZGFIlPX0lV4AHh40",
  authDomain: "todo-list-a58d4.firebaseapp.com",
  projectId: "todo-list-a58d4",
  storageBucket: "todo-list-a58d4.appspot.com",
  messagingSenderId: "1004231319638",
  appId: "1:1004231319638:web:fe91f230bb49944d5e1bf7",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);
