import { memo } from "react";

function CloseIcon({ color = "#000" }) {
  return (
    <svg
      version="1.1"
      viewBox="0 0 36 36"
      preserveAspectRatio="xMidYMid meet"
      xmlns="http://www.w3.org/2000/svg"
      focusable="false"
      role="img"
      width="100%"
      height="100%"
      fill={color}
      cursor="pointer"
    >
      <path d="M19.41,18l8.29-8.29a1,1,0,0,0-1.41-1.41L18,16.59,9.71,8.29A1,1,0,0,0,8.29,9.71L16.59,18,8.29,26.29a1,1,0,1,0,1.41,1.41L18,19.41l8.29,8.29a1,1,0,0,0,1.41-1.41Z" />
    </svg>
  );
}

export default memo(CloseIcon);
