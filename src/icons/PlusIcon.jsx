import { memo } from "react";

function PlusIcon({ color = "#000" }) {
  return (
    <svg
      version="1.1"
      viewBox="0 0 36 36"
      preserveAspectRatio="xMidYMid meet"
      focusable="false"
      role="img"
      width="100%"
      height="100%"
      fill={color}
      cursor="pointer"
    >
      <path d="M30,17H19V6a1,1,0,1,0-2,0V17H6a1,1,0,0,0-1,1,.91.91,0,0,0,1,.94H17V30a1,1,0,1,0,2,0V19H30a1,1,0,0,0,1-1A1,1,0,0,0,30,17Z" />
    </svg>
  );
}

export default memo(PlusIcon);
