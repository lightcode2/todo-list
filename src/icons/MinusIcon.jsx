import { memo } from "react";

function MinusIcon({ color = "#000000" }) {
  return (
    <svg
      version="1.1"
      viewBox="0 0 36 36"
      preserveAspectRatio="xMidYMid meet"
      xmlns="http://www.w3.org/2000/svg"
      focusable="false"
      role="img"
      width="100%"
      height="100%"
      fill={color}
      cursor="pointer"
    >
      <path d="M26,17H10a1,1,0,0,0,0,2H26a1,1,0,0,0,0-2Z" />
    </svg>
  );
}

export default memo(MinusIcon);
