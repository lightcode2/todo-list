import { memo } from "react";

function ActiveIcon({ color = "#0284c7" }) {
  return (
    <svg
      version="1.1"
      viewBox="0 0 36 36"
      preserveAspectRatio="xMidYMid meet"
      xmlns="http://www.w3.org/2000/svg"
      focusable="false"
      role="img"
      width="100%"
      height="100%"
      fill={color}
      cursor="pointer"
    >
      <path d="M18,34A16,16,0,1,1,34,18,16,16,0,0,1,18,34ZM18,4A14,14,0,1,0,32,18,14,14,0,0,0,18,4Z" />
      <path
        d="M18,34A16,16,0,1,1,34,18,16,16,0,0,1,18,34Z"
        style={{ display: "none" }}
      />
    </svg>
  );
}

export default memo(ActiveIcon);
